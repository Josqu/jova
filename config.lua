#!/usr/bin/lua

local config = {} -- config for jova

config.audio_path = 'audio/tmp/'


function config.sleep(n) -- callback function which is called when jova wants to pause.
  os.execute("sleep " .. tonumber(n))
end


function config.deepspeech(audio_file) -- callback function which called when deepspeech is needed.
   local models = '~/.deepspeech/models' -- Deepspeech model path
   local deepspeech = io.popen('source $HOME/tmp/deepspeech-venv/bin/activate; deepspeech ' .. models .. '/output_graph.pb ' .. config.audio_path .. audio_file .. ' ' .. models .. '/alphabet.txt ' , 'r')
   -- local deepspeech = io.popen( 'deepspeech --model ' .. models .. '/output_graph.pbmm ' ..  '--lm ' .. models .. '/lm.binary --trie '..models..'/trie --audio test.wav' , 'r') -- new version(?)
   return tostring(deepspeech:read("*a"))
end
function config.speak(text) -- callback function which is called when jova wants to speak
  os.execute('espeak ' .. text) 
  print('Said: ' .. text) 
end
function config.record(file_name) -- callback when jova wants to record stop if silenced
  local file_name = config.audio_path .. file_name
  os.execute('rec -c 1 -r 16k '.. file_name .. ' silence 1 0.1 3% 1 3.0 3% ')
end
function config.record_kill() -- callback when recording should be stoped(better solution needet cause killing all is not nice)
  os.execute('killall arecord')
end
function config.play(file) -- callback when jova wants to play a audio file
  os.execute('play ' .. file)
end


config.callword = 'cat' -- callword starting conservation.
config.scan_micro = 'source/scripts/scan_micro.bash' -- path to script for giving micro input volume. only work if returning numbers beetween 0.1 > 1 
config.klick_file = 'audio/klick.wav' -- audio while which its played when deepspeech is ready for input

return config
