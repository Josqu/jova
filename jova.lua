#!/usr/bin/lua
--x = io.popen('scan_micro', 'r')
--a = tonumber(x:read("*a"))
--print(a)

local jova = {}
local config = require('config')

function jova.hear() 
  if jova.micro_check() == true then
    if jova.detect_callword() == true then
      config.record('command.wav')
      local command = config.deepspeech('command.wav')
      print('command: ' .. command)
      return true
    end
  end
end


function jova.micro_check()
    local micro = io.popen(config.scan_micro, 'r')
    local micro_volume = tonumber(micro:read("*a"))
    if micro_volume == nil then
      print("Micro is offline")
    else
    if micro_volume > 0.168335 then
     print('Micro is loud ' .. micro_volume) return true
   else print('micro is silent ') end
   end
end

function jova.detect_callword()
   config.record('suspect.wav')
   x = config.deepspeech('suspect.wav') 
   print('suspect: ' .. x)
   if x == config.callword then
     return true
   end
end

function jova.update()
  jova.hear()
end

function jova.run()
  local i = 0 while i == 0 do
    jova.update()
  end
end

jova.run()
